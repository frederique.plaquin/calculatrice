class Model:
    def __init__(self):
        self.previous_value = ""
        self.value = ""
        self.operator = ""

    def calculate(self, widget):
        int_list = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        if widget.text == "C":
            self.previous_value = ""
            self.value = ""
            self.operator = ""
        elif widget.text == "+/-":
            self.value = self.value[1:] if self.value[0] == "-" else "-" + self.value
        elif widget.text == "%":
            value = float(self.value) if '.' in self.value else int(self.value)
            self.value = str(value / 100)
        elif widget.text == "=":
            value = self._evaluate()

            if '.0' in str(value):
                value = int(value)

            self.value = str(value)
        elif widget.text == '.':
            if not widget.text in self.value:
                self.value += widget.text
        elif widget.text in int_list:
            self.value += widget.text
        else:
            if self.value:
                self.operator = widget.text
                self.previous_value = self.value
                self.value = ""

        return self.value

    def _evaluate(self):
        return eval(self.previous_value+self.operator+self.value)
