from model import Model
from view import View


class Controller:
    def __init__(self):
        self.model = Model()
        self.view = View(self)

    def on_button_click(self, widget):
        result = self.model.calculate(widget)

        self.view.text_label.text = result
