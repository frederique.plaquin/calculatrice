import kivy
kivy.require('2.0.0')  # replace with your current kivy version !

from kivy.app import App

from controller import Controller


class PyCalcApp(App):

    def build(self):
        calculator = Controller()
        return calculator.view


if __name__ == '__main__':
    PyCalcApp().run()


