import kivy

import VARIABLES

kivy.require('2.0.0')  # replace with your current kivy version !

from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button


class Bouton(Button):
    def __init__(self, background_color, **kwargs):
        Button.__init__(self, **kwargs)
        self.background_color = background_color


class View(GridLayout):
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.rows = 2
        self.controller = controller
        self.grid1 = GridLayout(cols=1, rows=2)
        self.grid1.padding = 10
        self.add_widget(self.grid1)
        self.grid2 = GridLayout(cols=4, rows=5)
        self.grid2.padding = 10
        self._layout1()
        self.grid1.add_widget(self.grid2)

    def _layout1(self):
        self.text_label = Label(text="", size_hint=(1.0, 1.0), halign="right", valign="top", font_name="Arial",
                                font_size=40, size_hint_y=None, height=50, padding_x=10)
        self.text_label.bind(size=self.text_label.setter('text_size'))
        self.grid1.add_widget(self.text_label)
        self._make_buttons()

    def _make_buttons(self):
        num_list = [7, 8, 9, 4, 5, 6, 1, 2, 3, 0]
        operators_list = ['/', '*', '-', '+','=']
        for item in VARIABLES.button_captions:
            if item in num_list:
                self.button_color = "blue"
            elif item in operators_list:
                self.button_color = "red"
            else:
                self.button_color = "orange"
            button = Bouton(text=str(item), font_name="Arial", font_size=30, background_color=self.button_color)
            button.bind(on_press=self.controller.on_button_click)
            self.grid2.add_widget(button)
