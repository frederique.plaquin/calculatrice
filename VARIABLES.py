MAX_BUTTONS_PER_ROW = 4

button_captions = [
    'C', '+/-', '%', '/',
    7, 8, 9, '*',
    4, 5, 6, '-',
    1, 2, 3, '+',
    0, '.', '='
]